#!/bin/bash

while getopts 'n:r:h:*' flag; do
  case "${flag}" in
    n) namespace=("-n" "${OPTARG}") ;;
    r) release="${OPTARG}" ;;
    h) echo "Usage: $0 [-n namespace ] [ -r release]" >&2 ; exit 0 ;;
    *) echo -e "Illegal option: ${*} \nUsage: $0 [-n namespace ] [ -r release]" >&2 ; exit 0 ;;
  esac
done

if [ -z "$release" ] ; then
  release="gitlab"
fi

timestamp="$(date +%d%m%Y%H%M%S)"
archive_dir="./kubesos-$timestamp"
mkdir -p "$archive_dir"

# Verify requirements and write versions to file
echo "Getting kubectl version..."
kubectl_check="$(kubectl version 2> /dev/null)"

if [ -n "$kubectl_check" ]; then
    echo "$kubectl_check" > "$archive_dir"/kubectl-check
  else
    echo -e "ERROR: Could not load kubectl version, do you have kubectl installed?\n"
    exit 1
fi

# Check that kubectl version is > 1.14
# semver: $major.$minor.$patch(.$special)
kubectl_version_regex="^Client Version: v([0-9]+)\.([0-9]+)\.([0-9]+)\.*([0-9A-Za-z]*)"
kubectl_client_version="$(kubectl version --client --short)"
if [[ $kubectl_client_version =~ $kubectl_version_regex ]]; then
    kubectl_version_major=${BASH_REMATCH[1]}
    kubectl_version_minor=${BASH_REMATCH[2]}
    if [[ $kubectl_version_major -lt 1 || ( $kubectl_version_major -eq 1 && $kubectl_version_minor -lt 14 ) ]]; then
        echo "ERROR: This script requires kubectl v1.14+"
        exit 1
    fi
else
    echo "ERROR: Unrecognized kubectl client version: ${kubectl_client_version}"
    exit 1
fi

echo "Getting helm version..."
helm_version="$(helm version 2> /dev/null)"

if [ -n "$helm_version" ]; then
    echo "$helm_version" > "$archive_dir"/helm-version
  else
    echo -e "WARN: Could not load helm version, do you have helm installed?\n"
fi

helm_chart_version_command() {
    if grep "v2" <<< "$1"; then
        helm ls "$release" 2> /dev/null
    else
        helm "${namespace[@]}" ls | awk "\$1 ~ /""$release""/ { print \$0 }"
    fi
}

helm_get_values_command() {
    if grep "v2" <<< "$1"; then
        helm get values "$release" > "$archive_dir"/user_supplied_values.yaml
        helm get values --all "$release" > "$archive_dir"/all_values.yaml
    else
        helm "${namespace[@]}" get values "$release" > "$archive_dir"/user_supplied_values.yaml
        helm "${namespace[@]}" get values --all "$release" > "$archive_dir"/all_values.yaml
    fi
}

helm_get_history() {
	if grep "v2" <<< "$1"; then
		HELM_HISTORY=$(helm history "$release")
	else
		HELM_HISTORY=$(helm "${namespace[@]}" history "$release")
	fi
	echo "$HELM_HISTORY" | tee "$archive_dir"/helm_history
}

helm_get_values_by_revision() {
	HELM_HISTORY=$(helm_get_history "$helm_version")
	HELM_REVISIONS=$(tail -n +2 <<< "$HELM_HISTORY" | cut -f1 | tail -10)

	[[ $1 =~ v2 ]] && HELM_COMMAND="helm get values $release" || HELM_COMMAND="helm ""${namespace[*]}"" get values $release"

	for revision in $HELM_REVISIONS; do
		echo "Processing revision $revision..."
		$HELM_COMMAND --revision "$revision" --all > "$archive_dir"/all_values_rev_"$revision".yaml &
		$HELM_COMMAND --revision "$revision" > "$archive_dir"/user_supplied_values_rev_"$revision".yaml &
	done
}

echo "Getting GitLab chart version..."
chart_version=$(helm_chart_version_command "$helm_version")

if [ -n "$chart_version" ]; then
    echo "$chart_version" > "$archive_dir"/chart-version
  else
    echo -e "WARN: Unable to retrieve chart version\n"
fi

echo "Get pods..."
kubectl get pods "${namespace[@]}" > "$archive_dir"/get_pods &
echo "Top pods..."
kubectl top pods "${namespace[@]}" > "$archive_dir"/top_pods &
echo "Get jobs..."
kubectl get jobs "${namespace[@]}" > "$archive_dir"/get_jobs &
echo "Describe pods..."
kubectl describe pods "${namespace[@]}" > "$archive_dir"/describe_pods &
echo "Describe nodes..."
kubectl describe nodes "${namespace[@]}" > "$archive_dir"/describe_nodes &
kubectl top nodes "${namespace[@]}" > "$archive_dir"/top_nodes &
echo "Capture services and port mapping..."
kubectl get services "${namespace[@]}" > "$archive_dir"/get_services &
echo "Confirm services endpoints"
kubectl get endpoints "${namespace[@]}" > "$archive_dir"/get_endpoints &
echo "Describe ingress..."
kubectl describe ingress "${namespace[@]}" > "$archive_dir"/describe_ingress &
echo "Describe configmaps..."
kubectl describe configmaps "${namespace[@]}" > "$archive_dir"/configmaps &
echo "Get events..."
EVENT_OUTPUT_COLUMNS='custom-columns=CREATION_TIME:.metadata.creationTimestamp,FIRST_SEEN:.firstTimestamp,LAST_SEEN:.lastTimestamp,COUNT:.count,OBJECT:.involvedObject.kind,NAME:.involvedObject.name,TYPE:.type,REASON:.reason,MESSAGE:.message'
EVENT_SORT_BY=".lastTimestamp"
kubectl get events -o $EVENT_OUTPUT_COLUMNS --sort-by=$EVENT_SORT_BY "${namespace[@]}" > "$archive_dir"/events &
echo "Get deployments..."
kubectl get deployments "${namespace[@]}" > "$archive_dir"/get_deployments &
echo "Describe deployments..."
kubectl describe deployments "${namespace[@]}" > "$archive_dir"/describe_deployments &
echo "Retrieve list secrets (Content not included)..."
kubectl get secrets "${namespace[@]}" > "$archive_dir"/secrets &
echo "Get persistent volumes..."
kubectl get pv "${namespace[@]}" > "$archive_dir"/get_pv &
echo "Describe persistent volumes..."
kubectl describe pv "${namespace[@]}" > "$archive_dir"/describe_pv &
echo "Get persistent volumes claims..."
kubectl get pvc "${namespace[@]}" > "$archive_dir"/get_pvc &
echo "Describe persistent volume claims..."
kubectl describe pvc "${namespace[@]}" > "$archive_dir"/describe_pvc &
echo "Retrieve chart values..."
helm_get_values_command "$helm_version" &
echo "Retrieve revision values..."
helm_get_values_by_revision "$helm_version" &

# Get all app names from the app label
read -r -a app_names < <(kubectl get pods "${namespace[@]}" --output=jsonpath={.items..metadata.labels.app})
read -r -a distinct_app_names < <(printf "%s\n" "${app_names[@]}" | sort -u | tr '\n' ' ') 

# Retrieve logs for each component in array
for app in "${distinct_app_names[@]}"
do
  echo "Retrieve logs for $app..."
  kubectl logs --timestamps "${namespace[@]}" -l app="$app" --all-containers=true --max-log-requests=50 --tail=10000 --ignore-errors=true > "$archive_dir/$app.log" &
  if [ "$app" = "webservice" ]; then
    # special case - dump individual webservice and gitlab-workhorse logs
    kubectl logs --timestamps "${namespace[@]}" -l app="$app" --container=webservice --max-log-requests=50 --tail=10000 --ignore-errors=true > "$archive_dir/$app-webservice.log" &
    kubectl logs --timestamps "${namespace[@]}" -l app="$app" --container=gitlab-workhorse --max-log-requests=50 --tail=10000 --ignore-errors=true > "$archive_dir/$app-workhorse.log" &
  fi
done

# We have to wait for all of the background processes to finish before creating the archive
wait
archive_file="$archive_dir.tar.gz"
tar -czf "$archive_file" "$archive_dir"
echo "Archive file $archive_file created."
